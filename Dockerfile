FROM cern/cc7-base

# Volume where to mount the keytab as a secrets
# If credentials are passed as username and password with
# KEYTAB_USER and KEYTAB_PWD environment variables, a keytab will be
# generated and stored in KEYTAB_PATH.
#ENV KEYTAB_PATH '/var/lib/secrets'
# EOS credentials, this will be shared from the host
#ENV EOS_CREDENTIALS_PATH '/var/run/eosd/credentials'

#VOLUME ["${EOS_CREDENTIALS_PATH}"]
# Add EOS rpm repository to install necessary EOS related packages
#COPY eos.repo /etc/yum.repos.d/
# Install all necessary packages to run eosfusebind and the renewal of the kerberos ticket
#COPY run.sh /
#ENTRYPOINT ["/run.sh"]

RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;

VOLUME [ "/sys/fs/cgroup" ]

#Not needed on cc7-base
#RUN yum install -y cronie && yum clean all

RUN rm -rf /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Zurich /etc/localtime

COPY backup /etc/cron.daily
RUN chmod a+x /etc/cron.daily/backup

RUN echo "02 4 * * * /etc/cron.daily/backup" > /etc/crontab
RUN cat /etc/crontab

# ----- Install supercronic and configure the cronjob to prefetch from CVMFS ----- #
RUN yum -y install wget
RUN wget -q https://github.com/aptible/supercronic/releases/download/v0.1.12/supercronic-linux-amd64 -O /usr/bin/supercronic
RUN chmod +x /usr/bin/supercronic

CMD ["/usr/bin/supercronic","/etc/crontab"]
#CMD ["crond", "-n" ]
#CMD ["/usr/sbin/init"]
