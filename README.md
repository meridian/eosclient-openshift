### Overview

This Docker image takes care of binding user-provided kerberos credentials
(via keytab or username and password) and the periodic renewal of such credentials.

##### Requirements

The user needs to provide either of two:

* A keytab for the user accessing EOS mounted inside `/var/lib/secrets`
* Username and password of the user in `KEYTAB_USER` and `KEYTAB_PWD` environment
variables.

Optionally, an SELinux level (ref. ) can be passed as an argument in order to add
an extra level of protection to the credentials use by this user. The goal is to avoid
other applications on the host using EOS to access credentials of other apps.

#### Limitations

This image does not support to be run as root

#### Standalone example

```bash
docker run -d --user 1111 -v /var/run/eosd/credentials:/var/run/eosd/credentials \
           -e KEYTAB_USER=XXXX -e KEYTAB_PWD=XXXX gitlab-registry.cern.ch/paas-tools/eosclient-openshift:latest s0:c5,c10
```

#### OpenShift Example

First, create a secret containing username and password of the account to use for EOS access:

```bash
oc create secret generic eos-credentials --type=eos.cern.ch/credentials --from-literal=keytab-user=XXXX --from-literal=keytab-pwd=XXXX
```

To obtain an EOS PersistentVolumeClaim, simply create a PersistentVolumeClaim named `eos-volume` using
[storage class](https://docs.openshift.org/3.6/install_config/persistent_storage/dynamically_provisioning_pvs.html#install-config-persistent-storage-dynamically-provisioning-pvs) `eos` and mount it in your DeploymentConfig as `/eos`.

```bash
oc set volume dc/my-deployment-config --add --name=eos --type=persistentVolumeClaim --mount-path=/eos --claim-name=eos-volume --claim-class=eos --claim-size=1
```

The [EOS volume provisioner](https://gitlab.cern.ch/paas-tools/paas-infra/volume-provisioner-eos) for EOS
volumes will take care of creating the appropriate PersistentVolume in just a few seconds.

Once the `/eos` volume is provisioned, we still need to make the application use the credentials supplied in `eos-credentials`
to authenticate with EOS. This is done by adding a sidecar container that will take care of the EOS authentication.
For this, simply apply the [eosclient-container-patch.json patch](./eosclient-container-patch.json) to your DeploymentConfig.

**NB: the patch needs that the EOS credentials are in a secret named `eos-credentials` and the EOS volume PersistentVolumeClaim is
named `eos-volume`** (as explained above)

```bash
oc patch dc/my-deployment-config -p "$(cat eosclient-container-patch.json)"
```

Finally, be sure to set [health probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/)
to monitor that application has access to EOS and restart it if access to EOS is lost.

For this we need a path in `/eos` to monitor. Use a path that the application accesses as part of its normal operations
(typically the EOS workspace that the application reads/writes):

```bash
oc set probe dc/my-deployment-config --liveness --initial-delay-seconds=30 -- stat /eos/path/to/an/eos/folder/to/monitor
oc set probe dc/my-deployment-config --readiness -- stat /eos/path/to/an/eos/folder/to/monitor
```
