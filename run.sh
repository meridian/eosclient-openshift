#! /bin/sh
#
# The image supports the keytab to be passed either as mounted volume in $KEYTAB_PATH
# or the username and password of an account in KEYTAB_USER and KEYTAB_PWD

# Allow a custom SELinuxlevel to be passed as an argument. This will change the
# SELinux level on the credentials set by eosfusebind to add an extra layer of security
# and avoid applications using eos to change credentials of other apps.
SELINUX_CUSTOM_LEVEL=$1


cat /etc/cron.daily/backup

touch /eos/user/m/meridian/ELOG/mtd-elog_latest.tar.gz

while :
do
	sleep 1
done
